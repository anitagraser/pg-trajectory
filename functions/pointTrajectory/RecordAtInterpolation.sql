DROP FUNCTION IF EXISTS t_record_at_interpolated( trajectory, TIMESTAMP);
CREATE OR REPLACE FUNCTION t_record_at_interpolated(tr trajectory, t TIMESTAMP)
  RETURNS GEOMETRY AS
$BODY$

DECLARE
  tgp tg_pair;
  geom1 GEOMETRY;
  geom2 GEOMETRY;

BEGIN

  if tr.e_time < t OR tr.s_time > t THEN
    RETURN NULL;
  END IF;

  FOREACH tgp IN ARRAY tr.tr_data
  LOOP
    IF tgp.t < t THEN
      geom2 := tgp.g;
    END IF;
      geom1 := tgp.g;
  END LOOP;

  RETURN ST_LineInterpolatePoint(st_makeline(geom1, geom2), 0.5);

END

$BODY$
LANGUAGE 'plpgsql';
