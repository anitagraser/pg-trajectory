# README #

PG-Trajectory can be added to any PostGIS-enabled database. 
Use the PSQL shell to install PG-Trajectory as follows:

1. Open a PSQL shell to your PostGIS-enabled database (mine is called geolife): 

        C:\path\to\pg-trajectory> & 'C:\Program Files\PostgreSQL\9.4\bin\psql.exe' -d geolife -U postgres
       
2. Run the install script: 

        geolife-# \i install.sql
       
3. Now you can switch to PgAdmin and create a first test table:

        CREATE TABLE taxi_ride (id INTEGER PRIMARY KEY, from_loc TEXT, to_loc TEXT, traj Trajectory);
        INSERT INTO taxi_ride (id, from_loc, to_loc, traj) 
        VALUES (1, 'LA', 'NYC', 
          _trajectory(ARRAY[
            ROW('2017-12-03 00:00:00'::timestamptz,POINT(1,2))::tg_pair, 
            ROW('2017-12-03 00:01:00'::timestamptz,POINT(3,6))::tg_pair, 
            ROW('2017-12-03 00:02:00'::timestamptz,POINT(6,8))::tg_pair
            ]));
            
4. You can access trajectory attributes as follows:

        SELECT id, from_loc, to_loc, (traj).s_time, (traj).e_time, (traj).geom_type, st_astext((traj).bbox) 
        FROM taxi_ride 
        WHERE from_loc = 'LA' ;
      